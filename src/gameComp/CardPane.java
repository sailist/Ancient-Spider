package gameComp;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import gameElement.Card;
import tools.CardDispatcher;
import tools.DragManager;
import tools.ImageManager;
import tools.PlaceManager;
import tools.RankManager;
import tools.RuleManager;

public class CardPane extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3729946934724412581L;
	private Card card;
	private int group;
	private int floor;
	private int index;
	public CardPane(int group,int floor)
	{
		int x = PlaceManager.getPlaceManager().getXbyGroup(group);
		int y = PlaceManager.getPlaceManager().getYbyfloor(floor);
		this.setBounds(x, y, 100, 145);
		this.group = group;
		this.floor = floor;
		this.index = -1;
		ini();
		addListener();
		
	}
	public CardPane()
	{
		int x = PlaceManager.getPlaceManager().getExtraX(0);
		int y = PlaceManager.getPlaceManager().getExtraY();
		this.setBounds(x, y, 100, 145);
		this.index = 0;
		this.group = -1;
		this.floor = -1;
		ini();
		addListener();
		
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	private void ini()
	{
		CardDispatcher.getCardManager().dispatchCard(this);
	}
	public void setCard(Card card)
	{
		this.card = card;
	}
	public void showCard()
	{
		card.setHide(false);
		this.repaint();
	}
	public boolean canClick()
	{
		return !CardPane.this.card.isHide() && ((group == -1 && 
				DragManager.getDragManager().checkShowDiscardLast(index)) || group!=-1);
	}
	private void addListener()
	{
		
		MouseAdapter ma =new MouseAdapter() {
			CardPane[] drags;
			int startx,starty;
			int cx,cy;
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				if(!CardPane.this.canClick())
				{
					return ;
				}
				Point p = CardPane.this.getLocation();
				cx = p.x;
				cy = p.y;
				startx = e.getX();
				starty = e.getY();
				//确认是否应该显示，确认能否拖动
				
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				if(!CardPane.this.canClick())
				{
					return;
				}
				if(drags == null)
				{
					if(CardPane.this.getGroup() == -1)
					{
						drags = new CardPane[]{CardPane.this};
					}else
					{
						drags = DragManager.getDragManager().getCanDrag(CardPane.this);
					}
					DragManager.getDragManager().startDraging(drags);
				}
				if(drags == null)
				{
					return;
				}
				super.mouseDragged(e);
				int x = e.getX();
				int y = e.getY();
				for(int c = 0 ; c < drags.length;c++)
				{
					Point t = drags[c].getLocation();
					int px = t.x;
					int py = t.y;
					drags[c].setLocation(px+x-startx, py+y-starty);
				}
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				if(!CardPane.this.canClick())
				{
					return;
				}
				Point p = e.getComponent().getLocation();
//检查能否放置				
				DragManager.getDragManager().endDraging(drags, cx, cy, p.x, p.y);
				drags = null;
			}
		};
		
		this.addMouseListener(ma);
		this.addMouseMotionListener(ma);
		
	}
	public boolean isCardShow()
	{
		return !card.isHide();
	}
	
	private Image cardImg = null;
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if(card.isHide())
		{
			cardImg = ImageManager.getImageManager().getCardimg("b_1");
		}else
		{
			cardImg = ImageManager.getImageManager().getCardimg(card.getCtype()+"_"+card.getCnum());
		}
		if(cardImg!=null)
		{
			g.drawImage(cardImg, 1, 1, this.getWidth()-1,this.getHeight()-1,null);
		}
	}
	public int getGroup() {
		return group;
	}
	public void setGroup(int group)
	{
		this.group = group;
	}
	public int getFloor() {
		return floor;
	}
	@Override
	public String toString() {
		return "group:"+group+"_"+floor+"_" + card.toString();
	}
	public boolean place(CardPane tcard)
	{
		if((card.getCtype()+tcard.card.getCtype())%2 != 0 || RuleManager.IGNORE_TYPE)
		{
			if(card.getCnum() - tcard.card.getCnum() == 1 || RuleManager.IGNORE_NUM)
			{
				RankManager.getRankManager().addGrand(RankManager.POINT_DRAGOVER);
				return true;
			}
		}
		return false;
	}
	public void setFloor(int floor) {
		// TODO Auto-generated method stub
		this.floor = floor;
	}
	public Card getCard() {
		return card;
	}
	
}
