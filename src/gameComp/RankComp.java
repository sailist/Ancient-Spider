package gameComp;

import java.awt.Graphics;

public class RankComp extends ImageBaseComp{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2544643209621229089L;
	public RankComp()
	{
		super(ImageBaseComp.IMG_RANK);
		this.showLabel = false;
		TopButton tb = new TopButton(TopButton.RANK2GAME);
		this.add(tb);
		RankLabel r = new RankLabel();
		this.add(r);
		RankLabel rl = new RankLabel(1, "1", "00:00:00", "1");
		this.add(rl);
	}
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
	}

}
