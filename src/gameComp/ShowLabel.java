package gameComp;

import java.awt.Color;

import javax.swing.JLabel;

import tools.FontManager;
import tools.PlaceManager;
import tools.RankManager;

public class ShowLabel extends JLabel{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8129491126039984408L;
	public ShowLabel()
	{
		this.setFont(FontManager.getFontManager().getFont());
		this.setBounds(PlaceManager.getPlaceManager().getTimeX(),0,200,70);
		this.setForeground(Color.WHITE);
		this.setText("00:00:00");
		newTimegetter();
	}
	public ShowLabel(boolean flag)
	{
		this.setFont(FontManager.getFontManager().getFont());
		this.setBounds(PlaceManager.getPlaceManager().getGrandX(),0,200,70);
		this.setForeground(Color.WHITE);
		this.setText("0");
		RankManager.getRankManager().signGrandPane(this);
	}
	public void updataGrand(int grand)
	{
		this.setText(String.valueOf(grand));
	}
	public void newTimegetter()
	{
		new Thread() {
			@Override
			public void run() {
				super.run();
				while(true)
				{
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					ShowLabel.this.setText(RankManager.getRankManager().getTime());
				}
			}
		}.start();;
	}
}
