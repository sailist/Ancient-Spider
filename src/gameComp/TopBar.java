package gameComp;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import tools.FontManager;

public class TopBar extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3569055215385906861L;
	public static final int BAR_GAME = 0;
	public static final int BAR_RULE = 1;
	public static final int BAR_RANK = 2;
	public TopBar(int bartype)
	{
		this.transparency = 0.5f;
		this.setBounds(0, 0, 1200, 70);
		this.setLayout(null);
		this.setOpaque(false);
		if(bartype == BAR_RULE)
		{
			TopButton back = new TopButton(true);
			this.add(back);
		}
	}
	public TopBar()
	{
		this.transparency = 0.5f;
		this.setBounds(0, 0, 1200, 70);
		this.setLayout(null);
		this.setOpaque(false);
		
		this.add(new ShowLabel());
		this.add(new ShowLabel(true));
//		this.setBackground(Color.black);
		
		TopButton back = new TopButton(true);
		this.add(back);
		TopButton newGame = new TopButton();
		this.add(newGame);
	}
	private float transparency; 
    public void setTransparent(float transparency) {  
        this.transparency = transparency;  
    }  
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D graphics2d = (Graphics2D) g.create();  
        graphics2d.setComposite(AlphaComposite.SrcOver.derive(transparency));  
        graphics2d.setColor(Color.BLACK);  
        graphics2d.fillRect(0, 0, getWidth(), getHeight());
        
        paintText(g);
        
        graphics2d.dispose();  
	}
	private void paintText(Graphics g)
	{
		int y = 46;
		g.setColor(Color.white);
		g.setFont(FontManager.getFontManager().getFont());
		g.drawString("����", 630, y);
		g.drawString("ʱ��", 830, y);
	}
}


