package gameComp;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import tools.CardDispatcher;
import tools.FontManager;
import tools.GameManager;
import tools.ImageManager;

public class LoadComp extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1880533349903261264L;

	public LoadComp()
	{
		this.setBounds(0,0,1200,951);
		this.setBackground(new Color(17,55,102));

		waitLoaded();
	}
	public void waitLoaded()
	{
		new Thread() {

			@Override
			public void run() {
				super.run();
				ImageManager.getImageManager().iniCard();
				while(!ImageManager.getImageManager().isCardLoaded())
				{
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				GameManager.getGameManager().start();
				
			}}.start();;
	}
	public void wait2redispatch()
	{
		new Thread() {

			@Override
			public void run() {
				super.run();
				CardDispatcher.getCardManager().washCard();
				while(!ImageManager.getImageManager().isCardLoaded())
				{
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				GameManager.getGameManager().start();
				
			}}.start();;
	}
	 
	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		g.setColor(Color.WHITE);
		g.setFont(FontManager.getFontManager().getFont().deriveFont(50f));
		g.drawString("������...", 500, 400);
	}
}
