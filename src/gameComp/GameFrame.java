package gameComp;

import javax.swing.JFrame;
import javax.swing.JPanel;

import tools.GameManager;

public class GameFrame extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8830000355796816065L;
	public GameFrame() 
	{

		iniComp();
		
		this.setBounds(400,50,1185,850);
		this.setLayout(null);
		this.setTitle("ֽ����Ϸ");
		this.setResizable(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	JPanel p;
	private void iniComp() {
		p = new ImageBaseComp(ImageBaseComp.IMG_MAIN);
		
		
		this.add(p);
		p.setLayout(null);
		GameButton start = new GameButton(GameButton.START);
		p.add(start);
		GameButton rule = new GameButton(GameButton.RULE);
		p.add(rule);
//		GameButton rank = new GameButton(GameButton.RANK);
//		p.add(rank);
		GameButton exit = new GameButton(GameButton.EXIT);
		p.add(exit);
		
		GameManager.getGameManager().signGameFrame(this);
		GameManager.getGameManager().signStartMenu(p);
		
		
		RuleComp rp = new RuleComp();
		rp.setVisible(false);
		this.add(rp);
		GameManager.getGameManager().signRuleMenu(rp);
		
		RankComp rc = new RankComp();
		rc.setVisible(false);;
		this.add(rc);
		GameManager.getGameManager().signRankMenu(rc);
		
	}

}
