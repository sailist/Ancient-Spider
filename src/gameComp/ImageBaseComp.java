package gameComp;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

import tools.FontManager;
import tools.ImageManager;

public class ImageBaseComp extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5101792400216257638L;
	public static final int IMG_MAIN = 0;
	public static final int IMG_GAME = 1;
	public static final int IMG_RULE = 2;
	public static final int IMG_RANK = 3;
	public ImageBaseComp(int imgIndex)
	{
		this.setBounds(0, 0, 1200, 951);
		this.bgImg = ImageManager.getImageManager().getGameUI(imgIndex);
		this.setLayout(null);
	}
	protected boolean showLabel = true;
	private Image bgImg;
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(bgImg, 0, 0, null);
		if(showLabel)
		{
			g.setFont(FontManager.getFontManager().getFont());
//			g.drawString("Made by Sailist", 535, 700);
//			g.drawString("2018.4.21", 615, 750);
		}
		
	}
}
