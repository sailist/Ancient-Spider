package gameComp;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import tools.GameManager;
import tools.ImageManager;
import tools.PlaceManager;

public class GameButton extends JButton{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6819807556439531492L;
	public static final String START = "start";
	public static final String RULE = "rule";
	public static final String RANK = "rank";
	public static final String EXIT = "exit";
	public GameButton(String name)
	{
		if(name.equals(START))
		{
			this.setBounds(400
					, PlaceManager.getPlaceManager().getGameButtonY(0), 350, 300);
			this.setIcon(new ImageIcon(ImageManager.getImageManager().getStartButton()));
			iniStart();
		}else if(name.equals(RULE))
		{
			this.setBounds(PlaceManager.getPlaceManager().getGameButtonCenterX(), 
					PlaceManager.getPlaceManager().getGameButtonY(2), 350, 85);
			this.setIcon(new ImageIcon(ImageManager.getImageManager().getRuleButton()));
			iniRule();
		}else if(name.equals(RANK))
		{
			this.setBounds(650,
					PlaceManager.getPlaceManager().getGameButtonY(0), 350, 300);
			this.setIcon(new ImageIcon(ImageManager.getImageManager().getRankButton()));
			iniRank();
		}else if(name.equals(EXIT))
		{
			this.setBounds(PlaceManager.getPlaceManager().getGameButtonCenterX(),
					PlaceManager.getPlaceManager().getGameButtonY(3), 350, 85);
			this.setIcon(new ImageIcon(ImageManager.getImageManager().getExitButton()));
			iniExit();
		}
//		this.setContentAreaFilled(false);
	}
	/**
	 * 
	 * @param backFlag 为真为返回按钮，为假为新游戏按钮
	 */
	public GameButton(boolean backFlag)
	{
		
		if(backFlag)
		{
			
		}
	}
	private void iniStart()
	{
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				GameManager.getGameManager().ready();;
			}
			
		});
	}
	private void iniRank()
	{
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				GameManager.getGameManager().seeRank();
			}
			
		});
	}
	private void iniRule()
	{
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				GameManager.getGameManager().seeRule();
			}
			
		});
	}
	private void iniExit()
	{
		this.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				super.mouseClicked(e);
				System.exit(0);
			}
			
		});
	}
}
