package gameComp;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JLayeredPane;

import tools.DragManager;
import tools.ImageManager;

public class PlayComp extends JLayeredPane{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1315115538502075479L;
	public static final int HIDELAYER = 201;
	public static final int SHOWLAYER = 202;
	public static final int DRAGLAYER = 203;
	
	
	public PlayComp()
	{
		this.bgimg = ImageManager.getImageManager().getGameUI(ImageBaseComp.IMG_GAME);
		this.setBounds(0,0,1300,951);
		this.setVisible(true);
		this.setLayout(null);
		DragManager.getDragManager().signComp(this);
		iniCardPane();
	}
	public void reini()
	{
		iniCardPane();
	}
	private void iniCardPane() 
	{
		this.removeAll();
		for(int group = 0 ; group < 7; group++)
		{
			for(int floor = 0;floor <= group;floor++)
			{
				CardPane cp = new CardPane(group, floor);
				DragManager.getDragManager().signCard(cp, group, floor);
			}
		}
		for(int c = 0; c < 24;c++)
		{
			CardPane cp = new CardPane();
			DragManager.getDragManager().signCard(cp);
		}
		ClickPane cp = new ClickPane();
		this.add(cp);
		for(int c = 0; c < 4;c++)
		{
			PlacePane plc = new PlacePane(c);
			DragManager.getDragManager().signPlace(plc);
			this.add(plc);
		}
		
		TopBar tb = new TopBar();
		this.add(tb,300,0);
	}
	
	public void putToShow(CardPane card)
	{
		this.setLayer(card, SHOWLAYER, 0);
	}
	public void putToShow(CardPane[] card)
	{
		for(int c = 0 ;c < card.length;c++)
		{
			this.setLayer(card[c], SHOWLAYER, 0);
		}
	}
	public void DragToTop(CardPane card) 
	{
		this.setLayer(card, DRAGLAYER, 0);
	}
	public void DragToTop(CardPane[] card) 
	{
		for(int c = 0 ;c <card.length;c++)
		{
			this.setLayer(card[c], DRAGLAYER, 0);
		}
	}
	
	
	private Image bgimg = null;
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(bgimg, 0, 0,this.getWidth(),this.getHeight(), null);
	}
}
