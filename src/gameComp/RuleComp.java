package gameComp;

public class RuleComp extends ImageBaseComp{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5033967492386533583L;

	public RuleComp()
	{
		super(ImageBaseComp.IMG_RULE);
		this.showLabel = false;
		TopButton tb = new TopButton(TopButton.RULE2GAME);
		this.add(tb);
	}
}
