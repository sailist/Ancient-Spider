package gameComp;

import java.awt.Color;

import javax.swing.JLabel;

import tools.FontManager;

public class RankLabel extends JLabel{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7689376513698522677L;
	private String index;
	private String time;
	private String rank;
	public RankLabel(int floor,String index,String time,String grand)
	{
		String space1 = "     ";
		String space2 = space1+space1+space1+space1+space1+space1+space1+space1+space1+space1+space1+space1;
		this.setBounds(120, 150+60*floor, 900, 50);
		this.setFont(FontManager.getFontManager().getFont().deriveFont(35f));
		this.setForeground(Color.white);
		this.setText(index +space1+space1 +grand + space2 + time);
	}
	public RankLabel()
	{
		String space1 = "     ";
		String space2 = space1+space1+space1+space1+space1+space1+space1+space1+space1+space1+space1+space1;
		this.setBounds(120, 150, 900, 50);
		this.setFont(FontManager.getFontManager().getFont().deriveFont(35f));
		this.setForeground(Color.white);
		this.setText("名词"+space1+ "得分"+space2+"  时间");
	}
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	
}
