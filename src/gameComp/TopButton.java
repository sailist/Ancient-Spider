package gameComp;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;

import tools.FontManager;
import tools.GameManager;
import tools.RankManager;

public class TopButton extends JButton{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3046246160300227703L;
	public TopButton(boolean backflag)
	{
		this.setBounds(20, 0, 100, 70);
		this.setBorder(null);
		this.setBackground(Color.black);
		this.setForeground(Color.white);
		this.setFont(FontManager.getFontManager().getFont());
		this.setText("����");
		addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				super.mouseClicked(e);
				GameManager.getGameManager().backFromGame();
				RankManager.getRankManager().stop();
			}
		});
	}
	public TopButton()
	{
		this.setBounds(130, 0, 120, 70);
		this.setBorder(null);
		this.setBackground(Color.black);
		this.setForeground(Color.white);
		this.setFont(FontManager.getFontManager().getFont());
		this.setText("����Ϸ");
		
		addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				super.mouseClicked(e);
				GameManager.getGameManager().backFromGame();
				RankManager.getRankManager().stop();
				GameManager.getGameManager().ready();
			}
		});
	}
	public static final int RULE2GAME = 0;
	public static final int RANK2GAME = 1;
	public TopButton(int backtype)
	{
		this.setBounds(20, 0, 100, 70);
		this.setBorder(null);
		this.setBackground(Color.white);
		this.setForeground(Color.black);
		this.setFont(FontManager.getFontManager().getFont());
		this.setText("����");
		
		if(backtype == RULE2GAME)
		{
			addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					// TODO Auto-generated method stub
					super.mouseClicked(e);
					GameManager.getGameManager().backFromRule();
				}
			});
		}else if(backtype == RANK2GAME)
		{
			addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					// TODO Auto-generated method stub
					super.mouseClicked(e);
					GameManager.getGameManager().backFromRank();
				}
			});
		}
	}
}
