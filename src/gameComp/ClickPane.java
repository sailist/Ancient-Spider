package gameComp;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import tools.DragManager;
import tools.ImageManager;
import tools.PlaceManager;
import tools.S;

public class ClickPane extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2162101730382659875L;
	private int cardnum = 24;
	private int now = 1 ;
	public ClickPane()
	{
		this.bgImg = ImageManager.getImageManager().getClickUI();
		this.noImg = ImageManager.getImageManager().getNoClickUI();
		this.setOpaque(false);
		this.setBounds(PlaceManager.getPlaceManager().getClickX()
				,PlaceManager.getPlaceManager().getClickY(),100,145);
		this.setBackground(Color.BLUE);
		this.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				DragManager.getDragManager().showDiscard(ClickPane.this.nextBatch());
			}
		});
	}
	private boolean noCard = false;
	private void repaintCard(boolean flag)
	{
		if(noCard != flag)
		{
			noCard = flag;
			repaint();
		}
	}
	private int[] nextBatch()
	{
		int newNum = DragManager.getDragManager().getDiscardNum();
		S.p1(newNum,cardnum);
		if(newNum != cardnum)
		{
			this.cardnum = newNum;
			now-=3;
		}
		if(cardnum <= 3)
		{
			int[] indexs = new int[cardnum];
			for(int c = 0 ; c < indexs.length;c++)
			{
				indexs[c] = c;
			}
			return indexs;
		}
		if(cardnum - now >= 1)
		{
			int[] indexs = {now-1,now,now+1};
			this.setBackground(Color.BLUE);
			repaintCard(false);
			if(now+1 == cardnum-1)
			{
				
				repaintCard(true);
				repaint();
			}
			now +=3;
			return indexs;
		}else if(now > cardnum)
		{
			int[] indexs = null;
			now = 1;
			
			repaintCard(false);
			return indexs;
		}
		
		return null;
	}		
	public void minusnum()
	{
		this.cardnum -= 1;
	}
	private Image bgImg = null;
	private Image noImg = null;
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if(noCard)
		{
			g.drawImage(noImg, 0, 0, this.getWidth(),this.getHeight(),null);
		}else
		{
			g.drawImage(bgImg, 0, 0, this.getWidth(),this.getHeight(),null);
		}
	}
}
