package gameComp;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

import gameElement.Card;
import tools.ImageManager;
import tools.PlaceManager;
import tools.RankManager;

public class PlacePane extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5436012149209455200L;
	private int group;
	private int ctype = -1;
	private int cnum = 0;
	public PlacePane(int group)
	{
		this.bgImg = ImageManager.getImageManager().getPlaceUI();
		this.setGroup(group);
		this.setOpaque(false);
		this.setBounds(PlaceManager.getPlaceManager().getPlaceAreaX(group), 
				PlaceManager.getPlaceManager().getPlaceAreaY(), 100, 145);
	}
	
	public int getCtype() {
		return ctype;
	}
	public void setCtype(int ctype) {
		this.ctype = ctype;
	}
	public int getCnum() {
		return cnum;
	}
	public void setCnum(int cnum) {
		this.cnum = cnum;
	}
	public void updataCard(CardPane card )
	{
		Card c = card.getCard();
		if(c.getCnum() - cnum == 1)
		{
			if(c.getCtype() == ctype || ctype == -1)
			{
				cnum = c.getCnum();
				ctype = c.getCtype();
				RankManager.getRankManager().addGrand(RankManager.POINT_DRAG2PLACE);
				repaint();
				return;
			}
		}
		
	}
	private Image bgImg = null;
	private Image cardImg = null;
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if(cardImg == null)
		{
			cardImg = ImageManager.getImageManager().getCardimg("b_0");
		}
		if(ctype != -1)
		{
			cardImg = ImageManager.getImageManager().getCardimg(ctype+"_"+cnum);
		}

		if(cardImg!=null)
		{
			g.drawImage(cardImg, 1, 1, this.getWidth()-1,this.getHeight()-1,null);
		}else
		{
			g.drawImage(bgImg, 0, 0,this.getWidth(),this.getHeight(),null);
		}
	}

	public int getGroup() {
		return group;
	}

	public void setGroup(int group) {
		this.group = group;
	}
}
