package gameElement;

public class Card {
	private boolean isHide;
	private int ctype,cnum;
	public Card(boolean isHide, int ctype, int cnum) 
	{
		super();
		this.isHide = isHide;
		this.ctype = ctype;
		this.cnum = cnum;
	}
	
	public boolean isHide() {
		return isHide;
	}
	public void setHide(boolean isHide) {
		this.isHide = isHide;
	}
	public int getCtype() {
		return ctype;
	}
	public void setCtype(int ctype) {
		this.ctype = ctype;
	}
	public int getCnum() {
		return cnum;
	}
	public void setCnum(int cnum) {
		this.cnum = cnum;
	}
	@Override
	public String toString() {
		return ctype+"_"+cnum;
	}
}
