package tools;

import java.util.LinkedList;

import gameComp.CardPane;

public class CardList extends LinkedList<CardPane>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4291801949690139152L;


	public CardPane[] getClick2End(CardPane card)
	{
		int size = this.size();
		int c = this.indexOf(card);
		if(c == -1)
		{
		}
	
		for(;c < size;c++)
		{
			if(this.get(c).isCardShow())
			{
				break;
			}
		}
		
		CardPane[] cps = new  CardPane[size - c];
		int ic = c;
		for(;c < size;c++)
		{
			cps[c-ic] = this.get(c);
		}
		return cps;
	}
	public int getShowNum()
	{
		int size = this.size();
		int show = 0;
		for(int c = 0;c < size;c++)
		{
			if(this.get(c).isCardShow())
			{
				show++;
			}
		}
		return show;
	}
	public boolean moveTo(CardPane[] cards,CardList list,int group)
	{
		if(cards == null)
		{
			return false;
		}
		for(int c = 0 ;c < cards.length;c++)
		{
			this.remove(cards[c]);
		}
		
		for(int c = 0 ; c < cards.length; c++)
		{
			
			if(group == -1)
			{
				cards[c].setLocation(PlaceManager.getPlaceManager().getExtraX(cards[c].getIndex()),
						PlaceManager.getPlaceManager().getExtraY());
			}else
			{
				cards[c].setLocation(PlaceManager.getPlaceManager().getXbyGroup(group),
						PlaceManager.getPlaceManager().getYbyfloor(list.size(),list.getShowNum()));
				cards[c].setFloor(list.getShowNum());
			}
			cards[c].setGroup(group);
			list.add(cards[c]);
//			S.p(cards[c],group,"Set Group",PlaceManager.getPlaceManager().getXbyGroup(group));
			
		}
		if(!this.isEmpty() && !this.getLast().isCardShow())
		{
			this.getLast().showCard();
			
		}
		return true;
	}
	
	
	@Override
	public String toString() {
		String str = "";
		int size = this.size();
		for(int c = 0; c < size;c++)
		{
			str += this.get(c)+"|";
		}
		return str;
	}
}
