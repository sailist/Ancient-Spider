package tools;

/**
 * 转换坐标，提供组，层到界面的坐标
 * @author Sailist
 *
 */
public class PlaceManager 
{
	public static PlaceManager pm;
	public static PlaceManager getPlaceManager()
	{
		if(pm == null)
		{
			pm = new PlaceManager();
		}
		return pm;
	}
	private static final int UPX = 230;
	private static final int UPY = 90;
	private static final int DOWNY = 220+UPY;
	public static final int showY = 30;
	private static final int INTERNAL = 150;
	private static final double INTERNALD = INTERNAL;
	private static final int LEFTBOUND = 70;
	
	private static final int RIGHT_COLUMN = 600;
	private static final int GRAND_SHOW = RIGHT_COLUMN + 100;
	private static final int TIME_SHOW = RIGHT_COLUMN + 300;
	public int getXbyGroup(int group)
	{
		return LEFTBOUND+INTERNAL*group;
	}
	public int getGroupbyX(int x)
	{	
		double gd = (x-LEFTBOUND)/INTERNALD+0.5;
		if(gd - (int)gd < 0.2)
		{
			return -1;
		}
		return (int)gd;
	}
	public int getYbyfloor(int floor, int shownum)
	{
		int y = DOWNY;
		for(int c = 0;c < floor;c++)
		{
			if(shownum>0)
			{
				y += 30;
				shownum --;
			}else
			{
				y += 15;
			}
		}
		return y;
	}
	public int getDefaultRightX()
	{
		return UPX;
	}
	public int getYbyfloor(int floor)
	{
		return getYbyfloor(floor,0);
	}
	public int getExtraY()
	{
		return UPY;
	}
	public int getExtraX(int floor)
	{
		return UPX+floor*20;
	}
	public int getPlaceAreaX(int index)
	{
		return getXbyGroup(3)+INTERNAL*index;
	}
	public int getPlaceAreaY()
	{
		return UPY;
	}
	public int getClickX()
	{
		return LEFTBOUND;
	}
	public int getClickY()
	{
		return UPY;
	}
	public boolean inUpGroup(int x, int y)
	{
		if(y < 150 && PlaceManager.getPlaceManager().getGroupbyX(x)>2)
		{
			return true;
		}else
		{
			return false;
		}
	}
	
	public int getTimeX()
	{
		return TIME_SHOW;
	}
	public int getGrandX()
	{
		return GRAND_SHOW;
	}
	
	public int getGameButtonCenterX()
	{
		return 400;
	}
	public int getGameButtonY(int floor)
	{
		if(floor == 0)
		{
			return 100; 
		}else
		{
			return 260+100*floor;
		}
	}
}
