package tools;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class S {
	public static void p1(Object ...a) {
		for(int i = 0; i < a.length;i++)
		{
			System.out.print(a[i]+",");
		}
		System.out.println();
	}
	public static void p1(int[] a) {
		if(a == null)
		{
			System.out.print("");
			return;
		}
		for(int i = 0;i<a.length;i++)
		{
			System.out.print(a[i]+",");
		}
		System.out.println();
	}
	public static void log1(Object obj)
	{
		
		FileWriter fw = null;
		try {
		//如果文件存在，则追加内容；如果文件不存在，则创建文件
			File f=new File("./log.txt");
			fw = new FileWriter(f, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		PrintWriter pw = new PrintWriter(fw);
		pw.println(obj.toString());
		pw.flush();
		try {
			fw.flush();
			pw.close();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
