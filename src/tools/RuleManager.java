package tools;

import gameComp.CardPane;
import gameComp.PlacePane;

public class RuleManager {
	public static RuleManager rm;
	public static final boolean IGNORE_TYPE = false;
	public static final boolean IGNORE_PLACE_K = false;
	public static final boolean IGNORE_NUM = false;
	public static RuleManager getRuleManager()
	{
		if(rm == null)
		{
			rm = new RuleManager();
		}
		return rm;
	}
	private RuleManager(){}
	
	public boolean canPlace(CardList old,CardPane[] cards)
	{
		if(old.isEmpty())
		{ 
			if(cards[0].getCard().getCnum() == 12 || true)
			{
				return true;
			}
//			else
//			{
//				return false;
//			}
		}
		CardPane bigger = old.getLast();
		CardPane now = cards[0];
		return bigger.place(now);
	}

	public boolean canPlace(PlacePane place_Comp, CardPane card_Pane) {
		if(card_Pane.getCard().getCnum() - place_Comp.getCnum() == 1)
		{
			if(place_Comp.getCtype() == -1 || place_Comp.getCtype() == card_Pane.getCard().getCtype())
			{
				return true;
			}
		}
		return false;
	}

	
}
