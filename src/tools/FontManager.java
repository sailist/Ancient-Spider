package tools;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;

public class FontManager {
	public static FontManager fm;
	public static FontManager getFontManager()
	{
		if(fm == null)
		{
			fm = new FontManager();
		}
		return fm;
	}
	private Font defaultFont;
	public Font getFont()
	{
		if(defaultFont == null)
		{
			try {
				
				java.io.FileInputStream fi = new java.io.FileInputStream(new File("./SpiderCard_source/fonts/MSYH.TTC"));
				java.io.BufferedInputStream fb = new java.io.BufferedInputStream(fi);
				defaultFont = Font.createFont(Font.TRUETYPE_FONT, fb);
				defaultFont = defaultFont.deriveFont(Font.ROMAN_BASELINE, 30);
			} catch (FontFormatException | IOException e) {
				e.printStackTrace();
			}
		}
		return defaultFont;
	}
}
