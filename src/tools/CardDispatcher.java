package tools;

import java.util.LinkedList;
import java.util.Random;

import gameComp.CardPane;
import gameElement.Card;

public class CardDispatcher {
	public static CardDispatcher cm;
	public static CardDispatcher getCardManager()
	{
		if(cm == null)
		{
			cm = new CardDispatcher();
		}
		return cm;
	}
	private int ttype = -1,tnum= -1;
	private String[] kstrs = new String[]{"0_1","0_2","0_3","0_4","0_5","0_6","0_7","0_8","0_9","0_10","0_11","0_12","0_13","1_1","1_2","1_3","1_4","1_5","1_6","1_7","1_8","1_9","1_10","1_11","1_12","1_13","2_1","2_2","2_3","2_4","2_5","2_6","2_7","2_8","2_9","2_10","2_11","2_12","2_13","3_1","3_2","3_3","3_4","3_5","3_6","3_7","3_8","3_9","3_10","3_11","3_12","3_13"};
	private LinkedList<String> orderKeys = new LinkedList<String>();
	private LinkedList<String> randomKeys = new LinkedList<String>();
	private CardDispatcher()
	{
		washCard();
	}
	public void washCard()
	{
		for(int i = 0 ; i < kstrs.length;i++)
		{
			orderKeys.add(kstrs[i]);
		}
		Random r = new Random();
		for(int c = kstrs.length-1 ;c>=1;c--)
		{
			int index = r.nextInt(c);
			randomKeys.add(orderKeys.get(index));
			orderKeys.remove(index);
		}
		randomKeys.add(orderKeys.get(0));
	}
	public void dispatchCard(CardPane card_pane)
	{
		String[] cs = randomKeys.getFirst().split("_");
		randomKeys.removeFirst();
		Card card = new Card(true, Integer.valueOf(cs[0]), Integer.valueOf(cs[1]));
		card_pane.setCard(card);
	}
	public void dispatchCard1(CardPane card_pane)
	{
		if(ttype == -1 || tnum == -1)
		{
			ttype = 0;
			tnum = 0;
		}
		if(tnum>12)
		{
			ttype++;
			tnum = 0;
		}
		if(ttype > 3)
		{
			ttype = 0;
		}
		
		tnum++;
		Card card = new Card(true, ttype, tnum);
		card_pane.setCard(card);
	}
}
