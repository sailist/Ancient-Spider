package tools;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import gameComp.ShowLabel;

public class RankManager {
	public static RankManager rm;
	public static RankManager getRankManager()
	{
		if(rm == null)
		{
			rm = new RankManager();
		}
		return rm;
	}
	private long start;
	private int grand;
	public void start()
	{
		if(start == 0)
		{
			this.start = System.currentTimeMillis(); 
		}
		
	}
	public void stop()
	{
		String stime = getTime();
		String sgrand = String.valueOf(grand);
		FileWriter fw = null;
		try {
		//如果文件存在，则追加内容；如果文件不存在，则创建文件
			File f=new File("./SpiderCard_source/gameCatch/rank.txt");
			fw = new FileWriter(f, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		PrintWriter pw = new PrintWriter(fw);
		pw.println(stime+";"+sgrand);
		pw.flush();
		try {
			fw.flush();
			pw.close();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		start = 0;
		grand = 0;
	}
	public String getTime()
	{
		if(start == 0)
		{
			return "00:00:00";
		}
		long now = System.currentTimeMillis();
		long last = now-start;
		
//	    long days = last / (1000 * 60 * 60 * 24);  
	    long hours = (last % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60);  
	    long minutes = (last % (1000 * 60 * 60)) / (1000 * 60);  
	    long seconds = (last % (1000 * 60)) / 1000;  
//	    String dstr = String.format("%02d", days);
	    String hstr = String.format("%02d", hours);
	    String mstr = String.format("%02d", minutes);
	    String sstr = String.format("%02d", seconds);
	    return hstr + ":" + mstr + ":" + sstr ;
	}
	public static final int POINT_DRAGOVER = 5;
	public static final int POINT_DRAG2PLACE = 10;
	public void addGrand(int point)
	{
		this.grand += point;
		showLabel.updataGrand(grand);
	}
	public int getGrand()
	{
		return grand;
	}
	private ShowLabel showLabel;
	public void signGrandPane(ShowLabel showLabel) {
		this.showLabel = showLabel;
	}
	
	

}
