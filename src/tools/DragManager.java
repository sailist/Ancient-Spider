package tools;

import java.util.LinkedList;

import gameComp.CardPane;
import gameComp.PlacePane;
import gameComp.PlayComp;

public class DragManager {
	public static DragManager dm;
	public static DragManager getDragManager()
	{
		if(dm == null)
		{
			dm = new DragManager();
		}
		return dm;
	}
	private PlayComp pc;
	private CardList[] groups = new CardList[7];
	private CardList discard = new CardList();
	private CardList showDiscard = new CardList();
	private LinkedList<PlacePane> plcs = new LinkedList<PlacePane>();
	private DragManager()
	{
		for(int c = 0; c < groups.length;c++)
		{
			groups[c] = new CardList();
		}
	}
	public void reini()
	{
		groups = new CardList[7];
		for(int c = 0; c < groups.length;c++)
		{
			groups[c] = new CardList();
		}
		discard = new CardList();
		showDiscard = new CardList();
		plcs = new LinkedList<PlacePane>();
	}
	public void signComp(PlayComp pc)
	{
		this.pc = pc;
		
	}
	public void signCard(CardPane card,int group,int floor)
	{
		pc.add(card, PlayComp.HIDELAYER,0);
		groups[group].add(card);
		if(group == floor)
		{
			card.showCard();
		}
	}
	public void signPlace(PlacePane plc)
	{
		plcs.add(plc);
	}
	public void signCard(CardPane card)
	{
		pc.add(card,PlayComp.SHOWLAYER,0);
		discard.add(card);
		card.showCard();
		card.setVisible(false);
	}
	public CardPane[] getCanDrag(CardPane card)
	{
		return groups[card.getGroup()].getClick2End(card);
	}
	/**
	 * 检查该牌是最后一张牌
	 * @return
	 */
	public boolean checkIsLast(CardPane card)
	{
		int floor = groups[card.getGroup()].indexOf(card);
		return floor == groups[card.getGroup()].size()-1;
	}
	public boolean checkShowDiscardLast(int index)
	{
		return showDiscard.size() == index+1;
				
	}
	private boolean firstDrag = false;
	/**
	 * 检查能否放置在这个位置
	 */
	public void startDraging(CardPane[] cards)
	{
		if(!firstDrag)
		{
			RankManager.getRankManager().start();
		}
		pc.DragToTop(cards);
	}
	public void endDraging(CardPane[] card,int startx,int starty,int x ,int y)
	{
		if(card == null)
		{
			return;
		}
		int group = card[0].getGroup();
		int endGroup = PlaceManager.getPlaceManager().getGroupbyX(x);
		boolean inUpGroup = PlaceManager.getPlaceManager().inUpGroup(x,y);
		
		if(group == -1)//判断是否来自discard
		{
			if(endGroup != -1 && !inUpGroup )
			{
				if(RuleManager.getRuleManager().canPlace(groups[endGroup],card))
				{
					
					discard.moveTo(card, groups[endGroup], endGroup);
					showDiscard.remove(card[0]);
					return;
				}
			}
			if(inUpGroup)
			{
				if(card.length == 1)
				{
					if(RuleManager.getRuleManager().canPlace(plcs.get(endGroup-3),card[0]))
					{
						discard.remove(card[0]);
						plcs.get(endGroup-3).updataCard(card[0]);
						card[0].setVisible(false);
						showDiscard.remove(card[0]);
						return;
					}
				}
			} 
			
			discard.moveTo(card, discard, group);
			return;
		}
		
		//进行组别的判断
		//进行游戏规则的判断
		if(endGroup == -1 && !inUpGroup)
		{
			endGroup = group;
		}else
		{
			int py = PlaceManager.getPlaceManager().getYbyfloor(groups[endGroup].size(),groups[endGroup].getShowNum());
			if( Math.abs(py - y) > 100 && !inUpGroup)
			{
				endGroup = group;	
			}
		}
		if(inUpGroup)
		{
			if(card.length == 1)
			{
				if(RuleManager.getRuleManager().canPlace(plcs.get(endGroup-3),card[0]))
				{
					groups[group].remove(card[0]);
					card[0].setVisible(false);
					if(!groups[group].isEmpty()&&!groups[group].getLast().isCardShow())
					{
						groups[group].getLast().showCard();
					}
					plcs.get(endGroup-3).updataCard(card[0]);
					return;
				}
			}
		} 
		if(RuleManager.getRuleManager().canPlace(groups[endGroup],card))
		{
			groups[group].moveTo(card, groups[endGroup],endGroup);
		}else
		{
			groups[group].moveTo(card, groups[group],group);
		}
		
	}
	public void showDiscard(int[] indexs)
	{
		for(;!showDiscard.isEmpty(); )
		{
			showDiscard.removeFirst().setVisible(false);
		}
		if(indexs == null)
		{
			return;
		}
		for(int c = 0 ; c < indexs.length;c++)
		{
			if(indexs[c] >= discard.size())
			{
				continue;
			}
			CardPane cp = discard.get(indexs[c]);
			cp.setVisible(true);
			cp.setIndex(c);
			cp.setLocation(PlaceManager.getPlaceManager().getExtraX(c), 
					PlaceManager.getPlaceManager().getExtraY());
			showDiscard.add(cp);
		}
	}
	public int getDiscardNum()
	{
		return discard.size();
	}
	
}
