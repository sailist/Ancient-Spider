package tools;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;

import gameComp.ImageBaseComp;

public class ImageManager {
	public static ImageManager im;
	public static ImageManager getImageManager()
	{
		if(im == null)
		{
			im = new ImageManager();
		}
		return im;
	}
	private String card_dir = "./SpiderCard_source/cardimg/";
	private String ui_dir = "./SpiderCard_source/game_ui/";
	private boolean cardLoaded = false;
	public boolean isCardLoaded() {
		return cardLoaded;
	}
	public void setCardLoaded(boolean cardLoaded) {
		this.cardLoaded = cardLoaded;
	}
	private HashMap<String, BufferedImage> imgmap = new HashMap<>();
	
	private ImageManager()
	{
//		S.log(dir.getAbsolutePath());
		ini(ui_dir);
	}
	public void iniCard()
	{
		new Thread() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				super.run();
				ini(card_dir);
				cardLoaded = true;
			}}.start();
	}
	private void ini(final String dir)
	{
		File fdir = new File(dir);
		File[] filelist = fdir.listFiles();
		for(File f : filelist)
		{
			String name = f.getName();
			String key = name.split("\\.")[0];
			BufferedImage value = null;
			try {
				value = ImageIO.read(f);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			imgmap.put(key, value);
		}
	}
	
	
	private int ttype = -1,tnum= -1;
	public Image getCardimg(String key)
	{
//		if(imgmap.get(key) == null)
//		{
//			ini(card_dir);
//		}
		return imgmap.get(key); 
	}
	public Image getStartButton()
	{
//		if(imgmap.get("start_bt")==null)
//		{
//			ini(ui_dir);
//		}
		return imgmap.get("start_bt");
	}
	public Image getRuleButton()
	{
//		if(imgmap.get("rule_bt")==null)
//		{
//			ini(ui_dir);
//		}
		return imgmap.get("rule_bt");
	}	
	public Image getRankButton()
	{
//		if(imgmap.get("rank_bt")==null)
//		{
//			ini(ui_dir);
//		}
		return imgmap.get("rank_bt");
	}
	public Image getExitButton()
	{
		if(imgmap.get("exit_bt")==null)
		{
			ini(ui_dir);
		}
		return imgmap.get("exit_bt");
	}
	public Image getNext()
	{
		if(ttype == -1 || tnum == -1)
		{
			ttype = 0;
			tnum = 0;
		}
		if(tnum>12)
		{
			ttype++;
			tnum = 1;
		}
		if(ttype > 3)
		{
			ttype = 0;
		}
		
		tnum++;
		return imgmap.get(ttype + "_"+ tnum);
	}
	public Image getPlaceUI()
	{
		return imgmap.get("place_bg");
	}
	public Image getGameUI(int bgindex)
	{
		if(bgindex == ImageBaseComp.IMG_MAIN)
		{
			return imgmap.get("main_bg");
		}else if(bgindex == ImageBaseComp.IMG_GAME)
		{
			return imgmap.get("game_bg");
		}else if(bgindex == ImageBaseComp.IMG_RULE)
		{
			return imgmap.get("rule_bg");
		}else if(bgindex == ImageBaseComp.IMG_RANK)
		{
			return imgmap.get("rank_bg");
		}
		else {
			return null;
		}
	}
	public int[] getNow()
	{
		int[] c = {ttype,tnum};
		return c;
	}
	public Image getClickUI() {
		return imgmap.get("b_1");
	}
	public Image getNoClickUI() {
		return imgmap.get("click_bg");
	}
}
