package tools;

import javax.swing.JPanel;

import gameComp.GameFrame;
import gameComp.LoadComp;
import gameComp.PlayComp;

public class GameManager {
	private GameFrame gameFrame;
	private JPanel startMenu;
	private PlayComp playComp;
	private LoadComp loadPane;
	
	private JPanel ruleMenu;
	private JPanel rankMenu;
	private static GameManager gm;
	
	public static GameManager getGameManager()
	{
		if(gm == null)
		{
			gm = new GameManager();
		}
		return gm;
	}
	public void signStartMenu(JPanel startMenu)
	{
		this.startMenu = startMenu;
	}
	public void signRuleMenu(JPanel ruleMenu)
	{
		this.ruleMenu = ruleMenu;
	}
	public void signRankMenu(JPanel rankMenu)
	{
		this.rankMenu = rankMenu;
	}
	public void signGameMenu(PlayComp gameMenu)
	{
		this.playComp = gameMenu;
	}
	public void signGameFrame(GameFrame gameFrame)
	{
		this.gameFrame = gameFrame;
	}
	public void ready()
	{
		startMenu.setVisible(false);
		if(loadPane == null)
		{
			loadPane = new LoadComp();
			gameFrame.add(loadPane);
		}else
		{
			loadPane.setVisible(true);
			CardDispatcher.getCardManager().washCard();
			DragManager.getDragManager().reini();
			playComp.reini();
			start();
		}
	}
	public void seeRule()
	{
		startMenu.setVisible(false);
		ruleMenu.setVisible(true);
	}
	public void start()
	{
		
		if(playComp == null)
		{
			playComp = new PlayComp();
			gameFrame.add(playComp);
		}else
		{
			playComp.setVisible(true);
		}
		loadPane.setVisible(false);
	}
	public void backFromGame()
	{
		playComp.setVisible(false);
		startMenu.setVisible(true);
	}
	public void backFromRule() {
		ruleMenu.setVisible(false);
		startMenu.setVisible(true);
		
	}
	public void backFromRank() {
		// TODO Auto-generated method stub
		rankMenu.setVisible(false);
		startMenu.setVisible(true);
	}
	public void seeRank() {
		// TODO Auto-generated method stub
		rankMenu.setVisible(true);
		startMenu.setVisible(false);
	}
	
}
